import { DatabaseServiceService } from './database-service.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QuestionPaperPcmMarksService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if (ready) {
        this.databaseServiceService.getDataBase().executeSql(`CREATE TABLE IF NOT EXISTS questionPaper_pcm_marks
        (test_id integer primary key,p_mark1 integer,c_mark1 integer ,m_mark1 integer,year integer)`, []);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO questionPaper_pcm_marks
        (test_id,p_mark1,c_mark1,m_mark1,year)values(?,?,?,?,?)`,[1,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO questionPaper_pcm_marks
        (test_id,p_mark1,c_mark1,m_mark1,year)values(?,?,?,?,?)`,[2,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO questionPaper_pcm_marks
        (test_id,p_mark1,c_mark1,m_mark1,year)values(?,?,?,?,?)`,[3,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO questionPaper_pcm_marks
        (test_id,p_mark1,c_mark1,m_mark1,year)values(?,?,?,?,?)`,[4,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO questionPaper_pcm_marks
        (test_id,p_mark1,c_mark1,m_mark1,year)values(?,?,?,?,?)`,[5,0,0,0,0]);
      }
    })
  }
}
