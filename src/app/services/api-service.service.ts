import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class ApiServiceService {
  apiUrl: string = "https://cruncher-spring-boot.herokuapp.com/"
  // apiUrl: string = "http://192.168.43.46:8080/";
  constructor() {}
}
