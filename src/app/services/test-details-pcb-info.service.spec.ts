import { TestBed } from '@angular/core/testing';

import { TestDetailsPcbInfoService } from './test-details-pcb-info.service';

describe('TestDetailsPcbInfoService', () => {
  let service: TestDetailsPcbInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestDetailsPcbInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
