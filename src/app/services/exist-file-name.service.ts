import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class ExistFileNameService {

  constructor(private databaseServiceService: DatabaseServiceService) { }


  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if (ready) {
        this.databaseServiceService.getDataBase().executeSql(`CREATE TABLE IF NOT EXISTS ExistFileName
        (ID INTEGER PRIMARY KEY AUTOINCREMENT, FileName varchar(6000))`, []);
      }
    });
  }
}
