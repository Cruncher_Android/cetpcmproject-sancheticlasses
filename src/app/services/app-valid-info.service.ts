import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class AppValidInfoService {

  constructor(private databaseServiceService: DatabaseServiceService) { }


  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if(ready) {
        this.databaseServiceService.getDataBase().executeSql(`CREATE TABLE IF NOT EXISTS app_valid_info
        (appid varchar(255) primary key,inst_date long,exp_date long)`, []);
      }
    });
  }
}
