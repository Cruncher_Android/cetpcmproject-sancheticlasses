import { QuestionBookmarkService } from "./../../services/question-bookmark.service";
import { Storage } from "@ionic/storage";
import { ChapterInfoService } from "./../../services/chapter-info.service";
import { ActivatedRoute, Router, NavigationExtras } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { ToastController } from "@ionic/angular";

@Component({
  selector: "app-bookmark-chapters",
  templateUrl: "./bookmark-chapters.page.html",
  styleUrls: ["./bookmark-chapters.page.scss"],
})
export class BookmarkChaptersPage implements OnInit {
  subjectId: string;
  subjectName: string;
  chapterList: {
    chapter_id: number;
    subject_id: number;
    chapter_name: string;
    bookmarkedQue: boolean;
  }[] = [];
  bookmarkChapterList = [];

  loggedInType: string = "";

  constructor(
    private activatedRoute: ActivatedRoute,
    private chapterInfoService: ChapterInfoService,
    private router: Router,
    private storage: Storage,
    private questionBookmarkService: QuestionBookmarkService,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    // console.log("in bookmark chapters");
    this.storage.get("userLoggedIn").then((data) => {
      this.loggedInType = data;

      this.activatedRoute.paramMap.subscribe((subName) => {
        this.subjectId = subName.get("subjectId");
        this.subjectName = subName.get("subjectName");
        // console.log(this.subjectId);
        // console.log(this.subjectName);
        this.chapterInfoService
          .selectChapter(this.subjectId)
          .then((results) => {
            results.map((result) => {
              this.chapterList.push({
                chapter_id: result.chapter_id,
                subject_id: result.subject_id,
                chapter_name: result.chapter_name,
                bookmarkedQue: false,
              });
            });
            let bookmarkChapters = [];
            this.chapterList.map((chapter) => {
              bookmarkChapters.push(chapter.chapter_id);
            });
            this.questionBookmarkService
              .getBookmarkQueId(
                bookmarkChapters,
                this.loggedInType == "demo"
                  ? "question_bookmark_demo"
                  : "question_bookmark"
              )
              .then((result) => {
                this.bookmarkChapterList = result;
                this.bookmarkChapterList.map((bookmarkChapter) => {
                  this.chapterList.find(
                    ({ chapter_id }) => chapter_id == bookmarkChapter.chapterId
                  ).bookmarkedQue = true;
                });
              });
          });
      });
    });
  }

  async onChapterClick(id) {
    if (this.bookmarkChapterList.find(({ chapterId }) => chapterId == id)) {
      this.storage.set("bookChapterId", id).then((_) => {
        this.router.navigateByUrl("/test");
      });
    } else {
      let toast = await this.toastController.create({
        message: "No BookMark Questions Found",
        duration: 1500,
      });
      await toast.present();
    }
  }
}
