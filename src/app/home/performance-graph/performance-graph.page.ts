import { DatabaseServiceService } from "./../../services/database-service.service";
import { SaveTestInfoService } from "./../../services/save-test-info.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Chart } from "chart.js";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-performance-graph",
  templateUrl: "./performance-graph.page.html",
  styleUrls: ["./performance-graph.page.scss"],
})
export class PerformanceGraphPage implements OnInit {
  @ViewChild("lineChartPhy") lineChartPhy;
  @ViewChild("lineChartChem") lineChartChem;
  @ViewChild("lineChartMaths") lineChartMaths;
  @ViewChild("lineChartPCM") lineChartPCM;
  @ViewChild("lineChartQue") lineChartQue;
  chartPhy: any;
  chartChem: any;
  chartMaths: any;
  chartPCM: any;
  chartQue: any;

  phySelected: boolean;
  chemSelected: boolean;
  mathsSelected: boolean;
  pcmSelected: boolean;
  queSelected: boolean;
  selectedSubject: string;

  testDetailsPhy: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  testDetailsChem: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  testDetailsMaths: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  testDetailsPCM: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  testDetailsQue: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  testDetails: {
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
  }[] = [];

  loggedInType: string = "";

  constructor(
    private saveTestInfoService: SaveTestInfoService,
    private databaseServiceService: DatabaseServiceService,
    private storage: Storage
  ) {
    // console.log("performance graph page loaded");
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.storage.get("userLoggedIn").then((data) => {
      this.loggedInType = data;
      this.testDetailsPhy = [];
      this.testDetailsChem = [];
      this.testDetailsMaths = [];
      this.testDetailsPCM = [];
      this.testDetailsQue = [];
      this.testDetails = [];
      this.databaseServiceService.getDatabaseState().subscribe((ready) => {
        if (ready) {
          this.saveTestInfoService
            .getSubjectWiseTest(
              "Physics%",
              this.loggedInType == "demo"
                ? "save_test_info_demo"
                : "save_test_info"
            )
            .then((details) => {
              details.map((detail) => {
                this.testDetails.push(detail);
                this.testDetailsPhy.push(detail);
              });
              this.phySelected = true;
              this.chemSelected = false;
              this.mathsSelected = false;
              this.pcmSelected = false;
              this.queSelected = false;
              this.selectedSubject = "Physics";
              this.createLineChartForPhy();
            });

          this.saveTestInfoService
            .getSubjectWiseTest(
              "Chemistry%",
              this.loggedInType == "demo"
                ? "save_test_info_demo"
                : "save_test_info"
            )
            .then((details) => {
              details.map((detail) => {
                this.testDetailsChem.push(detail);
              });
            });

          this.saveTestInfoService
            .getSubjectWiseTest(
              "Mathematics%",
              this.loggedInType == "demo"
                ? "save_test_info_demo"
                : "save_test_info"
            )
            .then((details) => {
              details.map((detail) => {
                this.testDetailsMaths.push(detail);
              });
            });

          this.saveTestInfoService
            .getSubjectWiseTest(
              "PCM%",
              this.loggedInType == "demo"
                ? "save_test_info_demo"
                : "save_test_info"
            )
            .then((details) => {
              details.map((detail) => {
                this.testDetailsPCM.push(detail);
              });
            });

          this.saveTestInfoService
            .getSubjectWiseTest(
              "CET%",
              this.loggedInType == "demo"
                ? "save_test_info_demo"
                : "save_test_info"
            )
            .then((details) => {
              details.map((detail) => {
                this.testDetailsQue.push(detail);
              });
            });
        }
      });
    });
  }

  onPhyClick() {
    if (!this.phySelected) {
      this.testDetails = [];
      this.testDetailsPhy.map((detail) => this.testDetails.push(detail));
      this.selectedSubject = "Physics";
      this.phySelected = true;
      this.chemSelected = false;
      this.mathsSelected = false;
      this.pcmSelected = false;
      this.queSelected = false;
      this.createLineChartForPhy();
    }
  }

  onChemClick() {
    if (!this.chemSelected) {
      this.testDetails = [];
      this.testDetailsChem.map((detail) => this.testDetails.push(detail));
      this.selectedSubject = "Chemistry";
      this.phySelected = false;
      this.chemSelected = true;
      this.mathsSelected = false;
      this.pcmSelected = false;
      this.queSelected = false;
      this.createLineChartForChem();
    }
  }

  onMathsClick() {
    if (!this.mathsSelected) {
      this.testDetails = [];
      this.testDetailsMaths.map((detail) => this.testDetails.push(detail));
      this.selectedSubject = "Mathematics";
      this.phySelected = false;
      this.chemSelected = false;
      this.mathsSelected = true;
      this.pcmSelected = false;
      this.queSelected = false;
      this.createLineChartForMaths();
    }
  }

  onPCMClick() {
    if (!this.pcmSelected) {
      this.testDetails = [];
      this.testDetailsPCM.map((detail) => this.testDetails.push(detail));
      this.selectedSubject = "PCM";
      this.phySelected = false;
      this.chemSelected = false;
      this.mathsSelected = false;
      this.pcmSelected = true;
      this.queSelected = false;
      this.createLineChartForPCM();
    }
  }

  onQueClick() {
    if (!this.queSelected) {
      this.testDetails = [];
      this.testDetailsQue.map((detail) => this.testDetails.push(detail));
      this.selectedSubject = "CET";
      this.phySelected = false;
      this.chemSelected = false;
      this.mathsSelected = false;
      this.pcmSelected = false;
      this.queSelected = true;
      this.createLineChartForQue();
    }
  }

  createLineChartForPhy() {
    let chartData = [];
    let labels = [];
    this.testDetailsPhy.map((detail) =>
      chartData.push(
        Math.round((detail.obtainedMarks / detail.totalMarks) * 100)
      )
    );
    for (let i = 0; i < this.testDetailsPhy.length; i++) {
      labels.push(i + 1);
    }
    // console.log("chartData", chartData);
    this.chartPhy = new Chart(this.lineChartPhy.nativeElement, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            label: this.selectedSubject,
            data: chartData,
            backgroundColor: "#eb445a",
            // backgroundColor: 'rgba(0,0,0,0)'
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Marks(%)",
              },
              ticks: {
                beginAtZero: true,
                max: 100,
                stepSize: 20,
              },
              gridLines: {
                display: false,
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Tests",
              },
              gridLines: {
                display: false,
              },
            },
          ],
        },
      },
    });

    // console.log("teastDetailsPhy", this.testDetailsPhy);
  }

  createLineChartForChem() {
    let chartData = [];
    let labels = [];
    this.testDetailsChem.map((detail) =>
      chartData.push(
        Math.round((detail.obtainedMarks / detail.totalMarks) * 100)
      )
    );
    for (let i = 0; i < this.testDetailsChem.length; i++) {
      labels.push(i + 1);
    }
    // console.log("chartData", chartData);
    this.chartChem = new Chart(this.lineChartChem.nativeElement, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            label: this.selectedSubject,
            data: chartData,
            backgroundColor: "#eb445a",
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Marks(%)",
              },
              ticks: {
                beginAtZero: true,
                max: 100,
                stepSize: 20,
              },
              gridLines: {
                display: false,
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Tests",
              },
              gridLines: {
                display: false,
              },
            },
          ],
        },
      },
    });

    // console.log("teastDetailsChem", this.testDetailsChem);
  }
  createLineChartForMaths() {
    let chartData = [];
    let labels = [];
    this.testDetailsMaths.map((detail) =>
      chartData.push(
        Math.round((detail.obtainedMarks / detail.totalMarks) * 100)
      )
    );
    for (let i = 0; i < this.testDetailsMaths.length; i++) {
      labels.push(i + 1);
    }
    // console.log("chartData", chartData);
    this.chartMaths = new Chart(this.lineChartMaths.nativeElement, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            label: this.selectedSubject,
            data: chartData,
            backgroundColor: "#eb445a",
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Marks(%)",
              },
              ticks: {
                beginAtZero: true,
                max: 100,
                stepSize: 20,
              },
              gridLines: {
                display: false,
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Tests",
              },
              gridLines: {
                display: false,
              },
            },
          ],
        },
      },
    });

    // console.log("teastDetailsMaths", this.testDetailsMaths);
  }
  createLineChartForPCM() {
    let chartData = [];
    let labels = [];
    this.testDetailsPCM.map((detail) =>
      chartData.push(
        Math.round((detail.obtainedMarks / detail.totalMarks) * 100)
      )
    );
    for (let i = 0; i < this.testDetailsPCM.length; i++) {
      labels.push(i + 1);
    }
    // console.log("chartData", chartData);
    this.chartPCM = new Chart(this.lineChartPCM.nativeElement, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            label: this.selectedSubject,
            data: chartData,
            backgroundColor: "#eb445a",
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Marks(%)",
              },
              ticks: {
                beginAtZero: true,
                max: 100,
                stepSize: 20,
              },
              gridLines: {
                display: false,
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Tests",
              },
              gridLines: {
                display: false,
              },
            },
          ],
        },
      },
    });
    // console.log("teastDetailsPCM", this.testDetailsPCM);
  }
  createLineChartForQue() {
    let chartData = [];
    let labels = [];
    this.testDetailsQue.map((detail) =>
      chartData.push(
        Math.round((detail.obtainedMarks / detail.totalMarks) * 100)
      )
    );
    for (let i = 0; i < this.testDetailsQue.length; i++) {
      labels.push(i + 1);
    }
    // console.log("chartData", chartData);
    this.chartQue = new Chart(this.lineChartQue.nativeElement, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            label: this.selectedSubject,
            data: chartData,
            backgroundColor: "#eb445a",
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Marks(%)",
              },
              ticks: {
                beginAtZero: true,
                max: 100,
                stepSize: 20,
              },
              gridLines: {
                display: false,
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Tests",
              },
              gridLines: {
                display: false,
              },
            },
          ],
        },
      },
    });
    // console.log("teastDetailsQue", this.testDetailsQue);
  }
}
